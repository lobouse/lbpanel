# LBPanel

LBPanel is a free and open source desktop environment for Linux.

LBPanel is the standard panel of Lobo Desktop. The desktop panel can generate a menu for installed applications automatically from *.desktop files. It can be configured from a
preferences form.

<div align="center"> 
    <img src="img/screenshots/Screen_1.png" width="400px"</img> 
    <img src="img/screenshots/Screen_2.png" width="400px"</img> 
    <img src="img/screenshots/Screen_3.png" width="400px"</img> 
    <img src="img/screenshots/Screen_4.png" width="400px"</img> 
</div> 
